package com.myscript.atk.math.sample;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.myscript.atk.math.widget.MathWidgetApi;
import com.myscript.certificate.MyCertificate;

public class MainActivity extends Activity implements MathWidgetApi.OnConfigureListener,
        MathWidgetApi.OnRecognitionListener {
    private static final String TAG = "math";

    private static String inputComponent2 = "<span class=\"math-relative\">\n" +
            "    <span class=\"math-input\"></span>\n" +
            "    <span class=\"answer-result\"></span>\n" +
            "</span>\n" +
//            "<span class=\"math-relative\">\n" +
//            "    <span class=\"math-input\"></span>\n" +
//            "    <span class=\"answer-result\"></span>\n" +
            "</span>";
    private static String buttonComponent = "<p><input type=\"button\" onClick=\"getAllFieldValue()\" value=\"将所有输入框值传回Android\" style=\"width:180px; height:50px;\"></input></p>";

    private LMathView lMathView;
    private MathWidgetApi mWidget;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        // Set application title
        setTitle(getResources().getString(R.string.activity_name));

        mWidget = (MathWidgetApi) findViewById(R.id.math_widget);

        if (!mWidget.registerCertificate(MyCertificate.getBytes())) {
            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
            dlgAlert.setMessage("Please use a valid certificate.");
            dlgAlert.setTitle("Invalid certificate");
            dlgAlert.setCancelable(false);
            dlgAlert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    //dismiss the dialog
                }
            });
            dlgAlert.create().show();
            return;
        }

        String dir = "zip://" + getPackageCodePath() + "!/assets/conf/";
        Log.e(TAG, "dir:" + dir);
        mWidget.clearSearchPath();
        mWidget.addSearchDir(dir);
        mWidget.configure("math", "standard");
        mWidget.setOnConfigureListener(this);
        mWidget.setOnRecognitionListener(this);

        final View undoButton = findViewById(R.id.action_undo);
        if (undoButton != null) {
            undoButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    mWidget.undo();
                }
            });
        }

        final View redoButton = findViewById(R.id.action_redo);
        if (redoButton != null) {
            redoButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    mWidget.redo();
                }
            });
        }

        // Configure clear button
        final View clearButton = findViewById(R.id.action_clear);
        if (clearButton != null) {
            clearButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    mWidget.clear(true);
                }
            });
        }

        final View restoreButton = findViewById(R.id.action_restore);
        if (restoreButton != null) {
            restoreButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    lMathView.restoreAllMathField("1,2", "1,0");
                }
            });
        }

        final View allValueButton = findViewById(R.id.action_getAllValue);
        if (allValueButton != null) {
            allValueButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    lMathView.getAllFieldValues();
                }
            });
        }


        setInitialViews();
    }

    private void setInitialViews() {
        lMathView = (LMathView) findViewById(R.id.math_view);
        lMathView.setDisplayText(buttonComponent + inputComponent2);
    }

    @Override
    protected void onDestroy() {
        if (mWidget != null) {
            mWidget.release();
            mWidget = null;
        }
        super.onDestroy();
    }

    @Override
    public void onConfigureBegin(MathWidgetApi widget) {
    }

    @Override
    public void onConfigureEnd(MathWidgetApi widget, boolean success) {
        if (!success) {
            Toast.makeText(getApplicationContext(), widget.getErrorString(), Toast.LENGTH_LONG).show();
            Log.e(TAG, "Unable to configure the Math Widget: " + widget.getErrorString());
            return;
        }
        Toast.makeText(getApplicationContext(), "Math Widget Configured", Toast.LENGTH_SHORT).show();
        if (BuildConfig.DEBUG)
            Log.d(TAG, "Math Widget configured!");
    }

    @Override
    public void onRecognitionBegin(MathWidgetApi widget) {
    }

    @Override
    public void onRecognitionEnd(MathWidgetApi widget) {
        String latex = widget.getResultAsLaTeX();
        if (BuildConfig.DEBUG) {
//            Log.w(TAG, "Math Widget recognition:getResultAsText " + text + "\n");
//            Log.w(TAG, "Math Widget recognition:getResultAsMathXML " + mathML + "\n");
//            try {
//                lMathView.setMathField(latex);
//            } catch (UnsupportedEncodingException e) {
//                e.printStackTrace();
//            }
        }
    }
}
