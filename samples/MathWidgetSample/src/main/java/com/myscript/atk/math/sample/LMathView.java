package com.myscript.atk.math.sample;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class LMathView extends WebView {

    private String TAG = "math";

    private static final float default_text_size = 18;

    private Context context;
    private String display_text;
    private int text_color;
    private int text_size;

    public LMathView(Context context) {
        this(context, null);
    }

    public LMathView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        configurationSettingWebView();
        setDefaultTextColor(context);
        setDefaultTextSize();
    }

    private void configurationSettingWebView() {
        getSettings().setSupportZoom(true);
        getSettings().setJavaScriptEnabled(true);
        getSettings().setAllowFileAccess(true);
        getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        addJavascriptInterface(new WebAppInterface(context), "Android");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            setWebContentsDebuggingEnabled(true);
        }
        getSettings().setUseWideViewPort(true);
        getSettings().setLoadWithOverviewMode(true);
        getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        setHorizontalScrollBarEnabled(false);
        setVerticalScrollBarEnabled(false);
        setWebChromeClient(new WebChromeClient());
    }

    /**
     * 禁止键盘弹出
     *
     * @param enable
     */
    public void keyboardEnable(boolean enable) {
        this.setFocusable(enable);
    }

    public class WebAppInterface {
        Context context;

        WebAppInterface(Context c) {
            context = c;
        }

        @JavascriptInterface
        public void log(String message) {
            Log.e(TAG, message);
        }
    }

    public static int sp2px(Context context, float spVal) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP,
                spVal, context.getResources().getDisplayMetrics());
    }

    /**
     * 调用js方法返回所有页面输入框的值
     *
     * @return
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    public void getAllFieldValues() {
        evaluateJavascript("getAllFieldValue()", new ValueCallback<String>() {
            @Override
            public void onReceiveValue(String value) {
                // 返回所有页面输入框的值，结果以","分割
                value = value.replaceAll("\"", "");
                String[] v = value.split(",");
                Log.e(TAG, "String[] v size =" + v.length);
                for (String x : v) {
                    Log.e(TAG, "String[] v=" + x);
                }
                Log.e(TAG, "LMathView getAllFieldValues onReceiveValue:" + value);
            }
        });
    }

    /**
     * 还原所有用户的输入值
     *
     * @param userAnswers
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    public void restoreAllMathField(final String userAnswers, final String judgments) {
        if (null != userAnswers) {
            Log.e(TAG, "restoreUserAnswersToJs start: " + userAnswers);
            evaluateJavascript("restoreUserAnswersToJs('" + userAnswers + "')", new ValueCallback<String>() {
                @Override
                public void onReceiveValue(String value) {
                    Log.e(TAG, value);
                    if (null != judgments)
                        answerJudgments(judgments);
                }
            });
        }
    }

    /**
     * 用户答案的判定
     *
     * @param judgments
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    public void answerJudgments(final String judgments) {
        Log.e(TAG, "answerJudgmentToJs start: " + judgments);
        evaluateJavascript("answerJudgmentToJs('" + judgments + "')", new ValueCallback<String>() {
            @Override
            public void onReceiveValue(String value) {
                Log.e(TAG, value);
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public void setMathField(String latex) {
        if (TextUtils.isEmpty(latex))
            latex = "";
        latex = latex.split("=")[0];
        latex = string2Json(latex);
        Log.e(TAG, "setMathField:" + latex);
        evaluateJavascript("showLatexToJs('" + latex + "')", new ValueCallback<String>() {
            @Override
            public void onReceiveValue(String value) {

            }
        });
    }

    private String getOfflineKatexConfig() {
        String offline_config = getFromAssets("learnta/learnta.html");
        offline_config = offline_config
                .replace("{fontsize}", this.text_size + "")
                .replace("{textcolor}", getHexColor(this.text_color))
                .replace("{formula}", this.display_text);
        return offline_config;
    }

    public void setDisplayText(String formula_text) {
        this.display_text = formula_text;
        loadData();
    }

    public void setTextSize(int size) {
        this.text_size = size;
        loadData();
    }

    public void setTextColor(int color) {
        this.text_color = color;
        loadData();
    }

    private void setDefaultTextColor(Context context) {
        //sets default text color to black
        this.text_color = ContextCompat.getColor(context, android.R.color.black);
    }

    private void setDefaultTextSize() {
        //sets view default text size to 18
        this.text_size = (int) default_text_size;
    }

    public void setViewBackgroundColor(int color) {
        setBackgroundColor(color);
        this.invalidate();
    }

    private void loadData() {
        if (this.display_text != null) {
            this.loadDataWithBaseURL("null", getOfflineKatexConfig(), "text/html", "UTF-8", "about:blank");
        }
    }

    private String getHexColor(int intColor) {
        //Android and javascript color format differ javascript support Hex color, so the android color which user sets is converted to hexcolor to replicate the same in javascript.
        String hexColor = String.format("#%06X", (0xFFFFFF & intColor));
        Log.d(TAG, "Hex Color:" + hexColor);
        return hexColor;
    }

    private void pixelSizeConversion(float dimension) {
        if (dimension == default_text_size) {
            setTextSize((int) default_text_size);
        } else {
            int pixel_dimen_equivalent_size = (int) ((double) dimension / 1.6);
            setTextSize(pixel_dimen_equivalent_size);
        }
    }

    /**
     * 获取html文件
     */
    public String getFromAssets(String fileName) {
        try {
            InputStreamReader inputReader = new InputStreamReader(
                    getResources().getAssets().open(fileName));
            BufferedReader bufReader = new BufferedReader(inputReader);
            String line;
            String Result = "";
            while ((line = bufReader.readLine()) != null)
                Result += line;
            return Result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * JSON字符串特殊字符处理，比如：“\A1;1300”
     *
     * @param s
     * @return String
     */
    public static String string2Json(String s) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            switch (c) {
                case '\"':
                    sb.append("\\\"");
                    break;
                case '\\':
                    sb.append("\\\\");
                    break;
                case '/':
                    sb.append("\\/");
                    break;
                case '\b':
                    sb.append("\\b");
                    break;
                case '\f':
                    sb.append("\\f");
                    break;
                case '\n':
                    sb.append("\\n");
                    break;
                case '\r':
                    sb.append("\\r");
                    break;
                case '\t':
                    sb.append("\\t");
                    break;
                default:
                    sb.append(c);
            }
        }
        return sb.toString();
    }
}
